package br.com.neppo.lgpd.work;

import java.util.concurrent.CompletableFuture;

@FunctionalInterface
public interface Job<T, R> {
    CompletableFuture<R> work(T t);
}
