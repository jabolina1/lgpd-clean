package br.com.neppo.lgpd.work;

import br.com.neppo.lgpd.configuration.CleanerDataSource;
import br.com.neppo.lgpd.configuration.TenantContext;
import br.com.neppo.lgpd.configuration.model.BackupConfiguration;
import br.com.neppo.lgpd.configuration.model.SchemaConfiguration;
import br.com.neppo.lgpd.configuration.model.TableInformation;
import br.com.neppo.lgpd.util.JdbcAccess;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Slf4j
public class BackupJob implements Job<SchemaConfiguration, Void> {
    private final CleanerDataSource dataSource;
    private final SchemaConfiguration backupSchema;

    public BackupJob(CleanerDataSource dataSource, SchemaConfiguration backupSchema) {
        this.dataSource = dataSource;
        this.backupSchema = backupSchema;
    }

    @Override
    public CompletableFuture<Void> work(SchemaConfiguration schemaConfiguration) {
        TenantContext.tenantId(schemaConfiguration.getSchema());
        JdbcTemplate jdbcTemplate = new JdbcAccess(dataSource.determineTargetDataSource()).getJdbcTemplate();
        Map<String, Map<String, Object>> values = new HashMap<>();
        for (Map.Entry<String, TableInformation> table: schemaConfiguration.getTables().entrySet()) {
            TableInformation information = table.getValue();
            if (Boolean.TRUE.equals(information.getSkipInsert())) {
                continue;
            }

            Map<String, Object> value = readValues(jdbcTemplate, information);
            values.put(table.getKey(), value);
        }

        log.info("Read values for {}", schemaConfiguration.getSchema());
        TenantContext.tenantId(backupSchema.getSchema());
        jdbcTemplate = new JdbcAccess(dataSource.determineTargetDataSource()).getJdbcTemplate();
        for (Map.Entry<String, Map<String, Object>> entry: values.entrySet()) {
            TableInformation information = schemaConfiguration.getTables().get(entry.getKey());
            writeValues(jdbcTemplate, information, entry.getValue());
        }

        return null;
    }

    private Map<String, Object> readValues(JdbcTemplate jdbcTemplate, TableInformation information) {
        BackupConfiguration backup = information.getBackup();
        return jdbcTemplate.queryForMap(backup.getRead());
    }

    private void writeValues(JdbcTemplate jdbcTemplate, TableInformation information, Map<String, Object> values) {
        BackupConfiguration backup = information.getBackup();
        jdbcTemplate.update(backup.getWrite(), values);
    }
}
