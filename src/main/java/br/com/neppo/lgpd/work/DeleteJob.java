package br.com.neppo.lgpd.work;

import br.com.neppo.lgpd.configuration.TenantContext;
import br.com.neppo.lgpd.configuration.model.SchemaConfiguration;
import br.com.neppo.lgpd.configuration.model.TableInformation;
import br.com.neppo.lgpd.util.JdbcAccess;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Slf4j
public class DeleteJob implements Job<SchemaConfiguration, Void> {
    private final DataSource dataSource;

    public DeleteJob(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public CompletableFuture<Void> work(SchemaConfiguration schemaConfiguration) {
        TenantContext.tenantId(schemaConfiguration.getSchema());
        JdbcTemplate jdbcTemplate = new JdbcAccess(dataSource).getJdbcTemplate();
        jdbcTemplate.execute(createScript(schemaConfiguration.getTables()));
        return null;
    }

    private String createScript(Map<String, TableInformation> tables) {
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<String, TableInformation> table: tables.entrySet()) {
            builder.append(table.getValue().getDelete())
                    .append(";\n");
        }
        String sql = builder.toString();
        log.debug("Delete executing: {}", sql);
        return sql;
    }
}
