package br.com.neppo.lgpd.configuration.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Data
@Configuration
@ConfigurationProperties(prefix = "delete")
public class CleanerConfiguration {
    private Map<String, SchemaConfiguration> information;
    private String driverClassName;
    private SchemaConfiguration backup;
}
