package br.com.neppo.lgpd.configuration.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class BackupConfiguration {
    private String read;
    private String write;
}
