package br.com.neppo.lgpd.configuration.model;

import lombok.Data;
import lombok.ToString;

import java.util.Map;

@Data
@ToString
public class SchemaConfiguration {
    private String schema;
    private String url;
    private String user;

    @ToString.Exclude
    private String password;

    private Map<String, TableInformation> tables;
}
