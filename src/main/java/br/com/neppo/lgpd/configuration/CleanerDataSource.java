package br.com.neppo.lgpd.configuration;

import br.com.neppo.lgpd.configuration.model.CleanerConfiguration;
import br.com.neppo.lgpd.configuration.model.SchemaConfiguration;
import com.google.common.collect.Maps;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.Map;

@Slf4j
@Component
public class CleanerDataSource extends AbstractRoutingDataSource {
    private final CleanerConfiguration configuration;
    private final Map<String, DataSource> datasources;

    @Autowired
    public CleanerDataSource(CleanerConfiguration configuration) {
        this.configuration = configuration;
        this.datasources = Maps.newConcurrentMap();
    }

    @Override
    public void afterPropertiesSet() {
        buildDataSources();
    }

    @Override
    protected Object determineCurrentLookupKey() {
        return TenantContext.tenantId();
    }

    @Override
    public DataSource determineTargetDataSource() {
        String schema = (String) determineCurrentLookupKey();
        if (schema == null || schema.isEmpty()) {
            throw new RuntimeException("Could not determine the schema!");
        }

        return getOrCreate(schema);
    }

    private DataSource getOrCreate(String schema) {
        if (datasources.containsKey(schema)) {
            return datasources.get(schema);
        }

        SchemaConfiguration schemaConfiguration = configuration.getInformation().get(schema);
        if (schemaConfiguration == null) {
            log.error("Could not find data source nor configuration for {}", schema);
            throw new RuntimeException("Schema unknown: " + schema);
        }
        datasources.put(schema, buildDataSource(schemaConfiguration));
        return getOrCreate(schema);
    }

    private void buildDataSources() {
        for (Map.Entry<String, SchemaConfiguration> entry: configuration.getInformation().entrySet()) {
            datasources.computeIfAbsent(entry.getKey(), s -> buildDataSource(entry.getValue()));
        }
    }

    private DataSource buildDataSource(SchemaConfiguration schemaConfiguration) {
        log.info("Creating data source for schema {} with configuration {}", schemaConfiguration.getSchema(), schemaConfiguration);
        HikariConfig config = new HikariConfig();
        config.setSchema(schemaConfiguration.getSchema());
        config.setUsername(schemaConfiguration.getUser());
        config.setPassword(schemaConfiguration.getPassword());
        config.setJdbcUrl(schemaConfiguration.getUrl());
        config.setDriverClassName(configuration.getDriverClassName());
        config.setMaximumPoolSize(5);
        return new HikariDataSource(config);
    }
}
