package br.com.neppo.lgpd.configuration;

import lombok.extern.slf4j.Slf4j;

/**
 * Tenant context to hold the tenant id throughout a client request.
 * Each tenant will be held by a {@link ThreadLocal} class to isolate
 * the tenant information for each receiving request.
 */
@Slf4j
public final class TenantContext {
    private static ThreadLocal<String> tenantId = new ThreadLocal<>();

    public static void tenantId(String tenantId) {
        log.debug("Setting tenant id {}", tenantId);
        TenantContext.tenantId.set(tenantId);
    }

    public static String tenantId() {
        return TenantContext.tenantId.get();
    }
}
