package br.com.neppo.lgpd.configuration.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class TableInformation {
    private String delete;
    private BackupConfiguration backup;
    private Boolean skipInsert;
}
