package br.com.neppo.lgpd.util;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import javax.sql.DataSource;

public class JdbcAccess extends JdbcDaoSupport {
    public JdbcAccess(DataSource dataSource) {
        this.setDataSource(dataSource);
    }
}
